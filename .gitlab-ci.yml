# Copyright © 2018 Codelibre Consulting Limited

stages:
  - build
  - pages

variables:
  BUILD_DOXYGEN: "OFF"
  BUILD_EXTENDED_TESTS: "OFF"
  BUILD_SPHINX: "OFF"
  BUILD_SPHINX_LINKCHECK: "OFF"
  BUILD_SYSTEM: "Ninja"
  BUILD_VERBOSE: "OFF"
  BUILD_TYPE: "Debug"
  BUILD_DEP_BRANCH: "master"
  BUILD_DEPS: ""
  CMAKE: "cmake"
  CTEST: "ctest"
  CXX_STANDARD: "14"
  DEVTOOLSET: ""
  FILESYSTEM_IMPLEMENTATION: "boost"
  XMLDOM_IMPLEMENTATION: "xerces"

.unix_configure: &unix_configure
  before_script:
    - . "${CI_PROJECT_DIR}/.gitlab-ci/environment"
    - . "${CI_PROJECT_DIR}/.gitlab-ci/dependencies"
    - . "${CI_PROJECT_DIR}/.gitlab-ci/configure"

.win_configure: &win_configure
  before_script:
    - |
        . "${env:CI_PROJECT_DIR}/.gitlab-ci/environment.ps1"
        . "${env:CI_PROJECT_DIR}/.gitlab-ci/configure.ps1"

.unix_build: &unix_build
  script:
    - . "${CI_PROJECT_DIR}/.gitlab-ci/build"
  artifacts:
    name: $CI_PROJECT_NAME-$CI_JOB_NAME-bin
    paths:
    - dist

.unix_doc_build: &unix_doc_build
  script:
    - . "${CI_PROJECT_DIR}/.gitlab-ci/doc-build"

.win_build: &win_build
  script:
    - |
        . "${env:CI_PROJECT_DIR}/.gitlab-ci/build.ps1"
  artifacts:
    name: $CI_PROJECT_NAME-$CI_JOB_NAME-bin
    paths:
    - dist

build:ubuntu-18.04:c++14-boost:
  stage: build
  tags: [docker,linux]
  image: registry.gitlab.com/codelibre/containers/ome-files-build-ubuntu-18.04:latest
  variables:
    GTEST_ROOT: "/opt/gtest"
  <<: *unix_configure
  <<: *unix_build

build:ubuntu-18.04:c++14-noxerces:
  stage: build
  tags: [docker,linux]
  image: registry.gitlab.com/codelibre/containers/ome-files-build-ubuntu-18.04-noxerces:latest
  variables:
    GTEST_ROOT: "/opt/gtest"
    XMLDOM_IMPLEMENTATION: "qt5"
  <<: *unix_configure
  <<: *unix_build

build:ubuntu-18.04:c++14-qt:
  stage: build
  tags: [docker,linux]
  image: registry.gitlab.com/codelibre/containers/ome-files-build-ubuntu-18.04-noboost:latest
  variables:
    GTEST_ROOT: "/opt/gtest"
    FILESYSTEM_IMPLEMENTATION: "qt5"
    XMLDOM_IMPLEMENTATION: "qt5"
  <<: *unix_configure
  <<: *unix_build

build:ubuntu-16.04:c++14-boost:
  stage: build
  tags: [docker,linux]
  image: registry.gitlab.com/codelibre/containers/ome-files-build-ubuntu-16.04:latest
  variables:
    GTEST_ROOT: "/opt/gtest"
  <<: *unix_configure
  <<: *unix_build

build:centos-7:c++14-boost:
  stage: build
  tags: [docker,linux]
  image: registry.gitlab.com/codelibre/containers/ome-files-build-centos-7:latest
  variables:
    CMAKE: "cmake3"
    CTEST: "ctest3"
    DEVTOOLSET: "devtoolset-8"
  <<: *unix_configure
  <<: *unix_build

build:freebsd-12.0:c++17-boost:
  stage: build
  tags: [freebsd-12.0]
  variables:
    CXX_STANDARD: '17'
  <<: *unix_configure
  <<: *unix_build

build:freebsd-12.0:c++14-boost:
  stage: build
  tags: [freebsd-12.0]
  <<: *unix_configure
  <<: *unix_build

build:windows-vs2015:c++14-boost:
  stage: build
  tags: [windows,vs-2015]
  variables:
    CMAKE: 'C:\Program Files\CMake\bin\cmake'
    CTEST: 'C:\Program Files\CMake\bin\ctest'
    VCVER: 14
    VSVER: 2015
    VCVARSALL: 'C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat'
  <<: *win_configure
  <<: *win_build

build:windows-vs2017:c++17-boost:
  stage: build
  tags: [windows,vs-2017]
  variables:
    CMAKE: 'C:\Program Files\CMake\bin\cmake'
    CTEST: 'C:\Program Files\CMake\bin\ctest'
    CXX_STANDARD: '17'
    FILESYSTEM_IMPLEMENTATION: 'standard'
    VCVER: 15
    VSVER: 2017
    VCVARSALL: 'C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat'
  <<: *win_configure
  <<: *win_build

# Same as pages, but with optional linkchecking.
doc:
  stage: build
  tags: [docker,linux]
  image: registry.gitlab.com/codelibre/containers/ome-files-build-ubuntu-18.04:latest
  variables:
    BUILD_DOXYGEN: "ON"
    BUILD_SPHINX: "ON"
  <<: *unix_configure
  <<: *unix_doc_build
  allow_failure: true
  artifacts:
    name: $CI_PROJECT_NAME-$CI_JOB_NAME-doc
    paths:
    - public

pages:
  stage: pages
  tags: [docker,linux]
  image: registry.gitlab.com/codelibre/containers/ome-files-build-ubuntu-18.04:latest
  variables:
    BUILD_DOXYGEN: "ON"
    BUILD_SPHINX: "ON"
    BUILD_SPHINX_LINKCHECK: "OFF"
  <<: *unix_configure
  <<: *unix_doc_build
  artifacts:
    name: $CI_PROJECT_NAME-$CI_JOB_NAME
    paths:
    - public
  only:
  - master

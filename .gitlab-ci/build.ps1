$ErrorActionPreference = "Stop"

&$env:CMAKE --build .
if ($LastExitCode -ne 0) {
    exit $LastExitCode
}

&$env:CTEST --output-on-failure
if ($LastExitCode -ne 0) {
    exit $LastExitCode
}

&$env:CMAKE --build . --target install
if ($LastExitCode -ne 0) {
    exit $LastExitCode
}

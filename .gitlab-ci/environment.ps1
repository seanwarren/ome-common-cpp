$ErrorActionPreference = "Stop"

cmd /c 'call "%VCVARSALL%" x64 && set' | foreach {
    if ($_ -match "=") {
        $v = $_.split("="); set-item -force -path "ENV:\$($v[0])" -value "$($v[1])";
    }
}

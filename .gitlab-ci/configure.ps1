$ErrorActionPreference = "Stop"

mkdir b
if ($LastExitCode -ne 0) {
    exit $LastExitCode
}

cd b
if ($LastExitCode -ne 0) {
    exit $LastExitCode
}

&$env:CMAKE `
  -G "$env:BUILD_SYSTEM" `
  "-DCMAKE_BUILD_TYPE=$env:BUILD_TYPE" `
  "-DCMAKE_TOOLCHAIN_FILE=D:\vcpkg-vs${env:VSVER}\scripts\buildsystems\vcpkg.cmake" `
  "-DCMAKE_PREFIX_PATH:PATH=${env:CI_PROJECT_DIR}/deps/dist" `
  "-DCMAKE_INSTALL_PREFIX:PATH=${env:CI_PROJECT_DIR}/dist" `
  "-DCMAKE_CXX_STANDARD=${env:CXX_STANDARD}" `
  "-Dfilesystem=$env:FILESYSTEM_IMPLEMENTATION" `
  "-Ddoxygen:BOOL=$env:BUILD_DOXYGEN" `
  "-Dextended-tests:BOOL=$env:BUILD_EXTENDED_TESTS" `
  "-Dsphinx:BOOL=$env:BUILD_SPHINX" `
  "-Dxmldom=$env:XMLDOM_IMPLEMENTATION" `
  $env:DOXYGEN_TAGS `
  "-DGTEST_ROOT:PATH=D:\gtest-vs${env:VSVER}" `
  ..
if ($LastExitCode -ne 0) {
    exit $LastExitCode
}

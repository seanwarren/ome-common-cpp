# #%L
# OME C++ libraries (cmake build infrastructure)
# %%
# Copyright © 2006 - 2015 Open Microscopy Environment:
#   - Massachusetts Institute of Technology
#   - National Institutes of Health
#   - University of Dundee
#   - Board of Regents of the University of Wisconsin-Madison
#   - Glencoe Software, Inc.
# %%
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are
# those of the authors and should not be interpreted as representing official
# policies, either expressed or implied, of any organization.
# #L%

set(ome_xerces_util_headers
    EntityResolver.h
    ErrorReporter.h
    Platform.h
    String.h)

set(ome_xerces_util_dom_headers
    dom/Base.h
    dom/Document.h
    dom/Element.h
    dom/NamedNodeMap.h
    dom/Node.h
    dom/NodeList.h
    dom/Wrapper.h)

set(ome_xerces_util_all_headers
    ${ome_xerces_util_headers}
    ${ome_xerces_util_dom_headers})

set(ome_xerces_util_sources
    EntityResolver.cpp
    ErrorReporter.cpp
    Platform.cpp
    String.cpp
    dom/Document.cpp
    dom/NamedNodeMap.cpp
    dom/NodeList.cpp)

add_library(ome-xerces-util
    ${ome_xerces_util_sources}
    ${ome_xerces_util_all_headers})

target_include_directories(ome-xerces-util PUBLIC
                           $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/lib>
                           $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/lib>
                           $<BUILD_INTERFACE:${LibDl_INCLUDE_DIRS}>
                           $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)

target_link_libraries(ome-xerces-util
                      PUBLIC
                      OME::Common
                      XercesC::XercesC)

set_target_properties(ome-xerces-util PROPERTIES LINKER_LANGUAGE CXX)
set_target_properties(ome-xerces-util PROPERTIES VERSION ${ome-common_VERSION})
set_target_properties(ome-xerces-util PROPERTIES EXPORT_NAME "OME::XercesUtil")

add_library(OME::XercesUtil ALIAS ome-xerces-util)

if(WIN32)
  set(ome_xerces_util_config_dir "cmake")
else()
  set(ome_xerces_util_config_dir "${CMAKE_INSTALL_LIBDIR}/cmake/OMEXercesUtil")
endif()

install(TARGETS ome-xerces-util
        EXPORT OMEXercesUtilInternal
        RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
        ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
        COMPONENT "runtime"
        INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}")
install(EXPORT OMEXercesUtilInternal
        DESTINATION "${ome_xerces_util_config_dir}"
        COMPONENT "development")

include(CMakePackageConfigHelpers)
configure_package_config_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/OMEXercesUtilConfig.cmake.in"
  "${CMAKE_CURRENT_BINARY_DIR}/OMEXercesUtilConfig.cmake"
  INSTALL_DESTINATION "${ome_xerces_util_config_dir}")
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/OMEXercesUtilConfigVersion.cmake
  VERSION "${ome-xerces-util_VERSION}"
  COMPATIBILITY SameMajorVersion)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/OMEXercesUtilConfig.cmake
              ${CMAKE_CURRENT_BINARY_DIR}/OMEXercesUtilConfigVersion.cmake
        DESTINATION "${ome_xerces_util_config_dir}"
        COMPONENT "development")

set(ome_xerces_util_includedir "${CMAKE_INSTALL_INCLUDEDIR}/ome/xerces-util")

install(FILES ${ome_xerces_util_headers}
        DESTINATION ${ome_xerces_util_includedir}/
        COMPONENT "development")
install(FILES ${ome_xerces_util_dom_headers}
        DESTINATION ${ome_xerces_util_includedir}/dom
        COMPONENT "development")

# Dump header list for testing
header_include_list_write(ome_xerces_util_all_headers "" ome/xerces-util ${PROJECT_BINARY_DIR}/test/ome-xerces-util)

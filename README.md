# OME Common C++

[![pipeline status](https://gitlab.com/codelibre/ome-common-cpp/badges/master/pipeline.svg)](https://gitlab.com/codelibre/ome-common-cpp/commits/master)

Common functionality for OME C++ libraries and applications which is
not readily available from the C++ Standard Library.  This includes
basic portability functions, to wrapping other libraries to make them
usable with Modern C++ programming practices.

These libraries serve a similar purpose to the OME [ome-common-java
GitHub repository](https://github.com/ome/ome-common-java) Java
library, with some shared functionality, though for the most part they
have quite different functionality.

This repository is a fork of the OME [ome-common-cpp GitHub
repository](https://github.com/ome/ome-common-cpp) maintained by
Codelibre Consulting Limited.  It tracks upstream changes, and remains
API-compatible with upstream, and also provides additional features,
improvements and bugfixes not yet incorporated upstream on behalf of
its customers.

Documentation
-------------

- [C++ API reference](https://codelibre.gitlab.io/ome-common-cpp/api/)

Purpose
-------

OME Common's primary purpose is to ensure that a certain level of
basic functionality is provided for all platforms and compilers
supported by OME C++ projects.  It currently includes:

- Missing C++ standard library functionality:

  * regex
  * variant

- Missing Boost functionality:

  * Filesystem absolute path determination

- Extra functionality:

  * Boolean type for iterable 8-bit mask pixel data
  * logging
  * installation path determination
  * XML parsing (Xerces-C++ Modern C++ RAII wrappers)
  * XSL transforms (Xalan-C++ Modern C++ RAII wrappers)
  * Units of measurement (extended datatypes for all standard units
    used by the OME data model)

Development
-----------

Feature requests, bug reports and improvements should be submitted as
issues or merge requests against this repository.

Codelibre Consulting Limited provides software development consulting
services for this and other projects.  Please [contact
us](mailto:consulting@codelibre.net) to discuss your requirements.

The [upstream](https://github.com/ome/ome-common-cpp) repository is
not [currently accepting pull
requests](http://lists.openmicroscopy.org.uk/pipermail/ome-devel/2018-August/004294.html),
so issues and merge requests should be opened against this repository
in first instance.  Changes will be forwarded upstream as and when
this becomes possible.

Merge request testing
---------------------

We welcome merge requests from anyone.

Please verify the following before submitting a pull request:

 * verify that the branch merges cleanly into `master`
 * verify that the GitLab pipeline passes
 * verify that the branch only uses C++14 features (this should
   be tested by the pipeline)
 * make sure that your commits contain the correct authorship information and,
   if necessary, a signed-off-by line
 * make sure that the commit messages or pull request comment contains
   sufficient information for the reviewer to understand what problem was
   fixed and how to test it

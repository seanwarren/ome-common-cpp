# #%L
# OME C++ libraries (cmake build infrastructure)
# %%
# Copyright © 2018 Quantitative Imaging Systems, LLC
# %%
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation are
# those of the authors and should not be interpreted as representing official
# policies, either expressed or implied, of any organization.
# #L%

if (POLICY CMP0067)
  cmake_policy(SET CMP0067 NEW)
endif()
if (POLICY CMP0075)
  cmake_policy(SET CMP0075 NEW)
endif()

include(CheckCXXSourceCompiles)

set(filesystem "standard;boost;qt5" CACHE STRING "Use types from 'standard' (standard library), 'boost' or 'qt5'.  Multiple options will be used as fallbacks")

set(OME_FILESYSTEM NOTFOUND)

message(STATUS "Checking filesystem implementations: ${filesystem}")

foreach(fs ${filesystem})
  if (fs STREQUAL "standard")
    foreach(fslib "stdlib" "stdc++fs")
      set(CMAKE_REQUIRED_LIBRARIES_SAVE ${CMAKE_REQUIRED_LIBRARIES})
      if (NOT fslib STREQUAL "stdlib")
        set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES} ${fslib})
      endif()

      check_cxx_source_compiles(
        "#include <filesystem>

int main() {
  std::filesystem::path p{\"/foo\"};
}"
        OME_HAVE_FILESYSTEM_${fslib})
      set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES_SAVE})

      if (OME_HAVE_FILESYSTEM_${fslib})
        set(OME_HAVE_FILESYSTEM TRUE)
        set(OME_FILESYSTEM TRUE)
        if (NOT fslib STREQUAL "stdlib")
          set(FILESYSTEM_LIBS ${fslib})
        endif()
        message(STATUS "Using filesystem: standard")
        break()
      endif()
    endforeach()
  endif()
  if (fs STREQUAL "boost")
    set(Boost_USE_STATIC_LIBS OFF)
    set(Boost_USE_MULTITHREADED ON)
    set(Boost_USE_STATIC_LIBS OFF)
    find_package(Boost 1.53 COMPONENTS filesystem)
    if (TARGET Boost::filesystem)
      set(CMAKE_REQUIRED_DEFINITIONS_SAVE ${CMAKE_REQUIRED_DEFINITIONS})
      set(CMAKE_REQUIRED_DEFINITIONS ${CMAKE_REQUIRED_DEFINITIONS} -DBOOST_ALL_DYN_LINK -DBOOST_ALL_NO_LIB)
      set(CMAKE_REQUIRED_INCLUDES_SAVE ${CMAKE_REQUIRED_INCLUDES})
      set(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES} ${Boost_INCLUDE_DIRS})
      set(CMAKE_REQUIRED_LIBRARIES_SAVE ${CMAKE_REQUIRED_LIBRARIES})
      set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES} ${Boost_FILESYSTEM_LIBRARY_RELEASE} ${Boost_SYSTEM_LIBRARY_RELEASE})

      check_cxx_source_compiles(
        "#include <boost/filesystem.hpp>

int main() {
  boost::filesystem::path p{\"/foo\"};
}"
        OME_HAVE_BOOST_FILESYSTEM)

      check_cxx_source_compiles(
        "#include <boost/filesystem.hpp>

int main() {
  boost::filesystem::is_directory(\"/\");
}"
        BOOST_FILESYSTEM_LINK)

      if(OME_HAVE_BOOST_FILESYSTEM)
        set(CMAKE_REQUIRED_LIBRARIES_SAVE ${CMAKE_REQUIRED_LIBRARIES})
        set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES} ${Boost_FILESYSTEM_LIBRARY_RELEASE} ${Boost_SYSTEM_LIBRARY_RELEASE})
  # boost::filesystem in -lboost_filesystem
        check_cxx_source_compiles(
          "#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

int main() {
  boost::filesystem::path absolutefile = boost::filesystem::absolute(boost::filesystem::path(\"/tmp/../foobar\"));
}"
          OME_HAVE_BOOST_FILESYSTEM_ABSOLUTE)

        # boost::filesystem in -lboost_filesystem
        check_cxx_source_compiles(
          "#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

int main() {
  boost::filesystem::path canonicalfile = boost::filesystem::canonical(boost::filesystem::path(\"/tmp/../foobar\"));
}"
          OME_HAVE_BOOST_FILESYSTEM_CANONICAL)

        # boost::filesystem in -lboost_filesystem
        check_cxx_source_compiles(
          "#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

int main() {
  boost::filesystem::path relativefile = boost::filesystem::relative(boost::filesystem::path(\"/tmp/../foobar\"), boost::filesystem::path(\"/tmp\"));
}"
          OME_HAVE_BOOST_FILESYSTEM_RELATIVE)
      endif()
      set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES_SAVE})

      set(CMAKE_REQUIRED_LIBRARIES ${CMAKE_REQUIRED_LIBRARIES_SAVE})
      set(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES_SAVE})
      set(CMAKE_REQUIRED_DEFINITIONS ${CMAKE_REQUIRED_DEFINITIONS_SAVE})

      if (OME_HAVE_BOOST_FILESYSTEM)
        set(OME_FILESYSTEM TRUE)
        set(FILESYSTEM_LIBS Boost::filesystem)
        message(STATUS "Using filesystem: boost")
        break()
      endif()
    endif()
  endif()
  if (fs STREQUAL "qt5")
    find_package(Qt5Core)
    if (TARGET Qt5::Core)
      set(OME_HAVE_QT5_FILESYSTEM TRUE)
      set(OME_FILESYSTEM TRUE)
      set(FILESYSTEM_DEPS Qt5Core)
      set(FILESYSTEM_LIBS Qt5::Core)
      message(STATUS "Using filesystem: qt5")
    endif()
  endif()
  if(OME_FILESYSTEM)
    break()
  endif()
endforeach()

if(NOT OME_FILESYSTEM)
  message(FATAL_ERROR "No filesystem implementation found")
endif()
